> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 ADV Database Management 

## Tyler Bartlett

### Assignment 1 Requirements:

*Sub-Heading:*

1. AMPPS Installation
2. MySQL Workbench and SSH Login Procedures
3. ERD 
4. Setup local repository to Bitbucket servers

#### README.md file should include the following items:

* Screenshot of AMPPS
* Git commands with short descriptions
* ERD Image
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Assignment 1 Business Rules
	*Each employee may have one or more dependents.
	*Each employee has only one job.
	*Each job can be held by many employees.
	*Many employees may receive many benefits.
>
> #### Git commands w/short descriptions:

1. git init = Create an empty Git repository or reinitialize an existing one
2. git status = Show the working tree status
3. git add = Add file contents to the index
4. git commit = Record changes to the repository
5. git push = Update remote refs along with associated objects
6. git pull = Fetch from and integrate with another repository or a local branch
7. git log = Show commit logs

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of ERD*:

![ERD Screenshot](img/a1erd.png)

*Screenshot of A1 Ex 1* :

![SQL Code Screenshot] (img/a1ex1.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
