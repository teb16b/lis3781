# LIS3781 ADV Database Management

## Tyler Bartlett

### Assignment 3 Requirements:

*Sub-Heading:*

1. Using Remote Labs
2. Populate Tables
3. SQL Statements

#### README.md file should include the following items:

*ERD Screenshot

#### Assignment Screenshots:

**Screenshot of ERD**:

![ERD Screenshot](img/erd.png)
