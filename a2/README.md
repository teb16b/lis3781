# LIS3781 ADV Database Management 

## Tyler Bartlett

### Assignment 2 Requirements:

*Sub-Heading:*

1. Create populated tables with only SQL code
2. Forward Engineer to the CCI Server

#### README.md file should include the following items:

* Screenshot of non-QCITR.com watermark SQL Code.
* Screenshot of non-QCITR.com populated tables;

#### Assignment Screenshots:

***Screenshot of SQL Code PT.1***:

![Part 1 of SQL Code](img/SQLCodePt1.png)

***Screenshot of SQL Code PT. 2***:

![Part 2 of SQL Code](img/SQLCodePt2.png)

***Screenshot of Populated Tables***:

![Android Studio Installation Screenshot](img/PopTables.png)