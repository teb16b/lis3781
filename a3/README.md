> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 ADV Database Management

## Tyler Bartlett

### Assignment 3 Requirements:

*Sub-Heading:*

1. Tables & Data: Using only SQL (Oracle)
2. SQL Solutions

#### README.md file should include the following items:

* Screenshot of SQL Code
* Screenshot of Populated Table

#### Assignment Screenshots:

**Screenshot of SQL Code and Populated Tables**:

![Sql and table](img/sqlcode.png)
