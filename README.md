> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 ADV Database Management

## Tyler Bartlett

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- AMPPS Installation
	- MySQL Workbench and SSH Login Procedures
	- ERD 
	- Setup local repository to Bitbucket servers
2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create populated tables with only SQL code
	- Forward Engineer to the CCI Server
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Tables & Data: Using only SQL (Oracle)
	- SQL Solutions
4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Using Remote Labs
	- Populate Tables
	- SQL Statements
5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Using Remote Labs
	- Add and edit tables in exsisting database
	- SQL Statements 
6. [P1 README.md](p1/README.md "My P1 README.md file")
	- ERD
	- SQL Statement Questions
7. [P2 README.md](p2/README.md "My P2 README.md file")
	- MongoDB Shell Commands
	- Screenshots

